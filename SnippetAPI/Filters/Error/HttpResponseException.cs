using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebApplication2.Filters.Error
{
    public class ErrorModel
    {
        public ErrorModel()
        {
        }

        public ErrorModel(int StatusCode, string? Message = null, string? InternalMessage = null)
        {
            this.Message = Message;
            this.InternalMessage = InternalMessage;
            this.StatusCode = StatusCode;
        }
        public int StatusCode { get; set; }
        public string? Message { get; set; }
        [JsonIgnore]
        public string? InternalMessage { get; set; }

    }
}