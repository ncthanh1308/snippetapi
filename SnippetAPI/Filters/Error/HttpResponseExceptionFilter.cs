using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApplication2.Filters.Error
{
    public class HttpResponseExceptionFilter : IExceptionFilter
{
     public void OnException(ExceptionContext context)
        {
            var error = new ErrorModel((int)HttpStatusCode.InternalServerError, context.Exception.Message, context.Exception.InnerException?.Message);
            context.Result = new ObjectResult(error)
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
            context.ExceptionHandled = true;
        }
    }
}