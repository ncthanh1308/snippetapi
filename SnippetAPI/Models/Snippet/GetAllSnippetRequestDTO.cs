using SnippetAPI.Models;

namespace WebApplication2.Models.Snippet
{
    public class GetAllSnippetRequestDTO: PagedModel
    {
        public string? Search { get; set; }
    }
}