namespace SnippetAPI.Models
{
    public class PagedModel
    {
        public int? PageSize { get; set; } = 10;
        public int? PageNumber { get; set; } = 1;
    }

    public class PageResultBase
    {
         public int CurrentPage { get; set; } 
        public int PageCount { get; set; } 
        public int PageSize { get; set; } 
        public int RowCount { get; set; }

        public int FirstRowOnPage
        {
    
            get { return (CurrentPage - 1) * PageSize + 1; }
        }
    
        public int LastRowOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, RowCount); }
        }

        
    }
    public class PagedResult<T> : PageResultBase where T : class
    {
        public IList<T> Results { get; set; }
    
        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}